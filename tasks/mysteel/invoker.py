"""
@author  : MG
@Time    : 2020/12/3 16:06
@File    : invoker.py
@contact : mmmaaaggg@163.com
@desc    : 用于我的钢铁 invoker
"""
import json

import requests
from ibats_utils.mess import date_2_str
from requests import Response

from tasks.config import config


def api_invoker(index_codes, start_time, end_time, order='asc') -> Response:
    """
    例如：{"indexCodes":["ID00034707"],"order":"desc","startTime":"2010-09-10","endTime":"2020-09-10"}
    :param index_codes:jsonArray 指标编码（最多50个，必填）
    :param start_time:String 数据开始时间（yyyy-MM-dd，必填）
    :param end_time:String 数据结束时间（yyyy-MM-dd，必填）
    :param order:String 数据排序规则（desc/asc，必填）
    :return:
    """
    json_str = json.dumps({
        "indexCodes": index_codes,
        "order": order,
        "startTime": date_2_str(start_time),
        "endTime": date_2_str(end_time),
    })
    print(json_str)
    response = requests.post(
        config.MY_STEEL_QUERY_URL,
        json=json_str,
        headers={
            "accessTokenSign": config.MY_STEEL_ACCESS_TOKEN_SIGN,
        },
    )
    print(response.content.decode('utf-8'))
    return response


def _test_api_invoker():
    response = api_invoker(["ID00034707"], start_time="2020-11-20", end_time="2020-12-01")
    # print(response.content)


if __name__ == "__main__":
    _test_api_invoker()
