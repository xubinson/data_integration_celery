"""
@author  : MG
@Time    : 2020/12/3 10:47
@File    : future_mid_day_task.py
@contact : mmmaaaggg@163.com
@desc    : 用于
"""
import logging

from tasks import config
from tasks.wind.futures.future import import_future_min, get_main_secondary_contract_by_instrument_types
from tasks.wind.to_vnpy import min_to_vnpy_increment

logger = logging.getLogger()


def run_mid_day_task():
    wind_code_set = None
    # wind_code_set = (
    #     'RB2105.SHF', 'RB2110.SHF',
    #     'HC2105.SHF', 'HC2110.SHF',
    # )
    instrument_types = list({
        'a', 'ap', 'b', 'bu', 'c', 'cf', 'cj', 'cs', 'cy', 'eb', 'eg', 'fg',
        'hc', 'i', 'ic', 'if', 'ih', 'j', 'jd', 'jm', 'l', 'm', 'ma', 'nr',
        'oi', 'p', 'rb', 'rm', 'ru', 'sc', 'sp', 'sr', 'ss', 'ta', 'v', 'y',
        'zc', 'zn',
    })
    wind_code_set = get_main_secondary_contract_by_instrument_types(instrument_types=instrument_types)
    import_future_min(wind_code_set=wind_code_set, )
    min_to_vnpy_increment(
        start_vnp=True,
    )
    logger.info("all task finished")


def to_vnpy():
    logger.info("min_to_vnpy_increment start")
    min_to_vnpy_increment(
        db_schema_name=config.DB_SCHEMA_VNPY,
        # instrument_types=['rb', 'i', 'hc'],
    )
    min_to_vnpy_increment(
        db_schema_name=config.DB_SCHEMA_VNPY_PA,
        # instrument_types=['rb', 'i', 'hc'],
        start_vnp=True,
    )
    logger.info("all task finished")


if __name__ == "__main__":
    run_mid_day_task()
    # to_vnpy()
