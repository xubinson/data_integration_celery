#! /usr/bin/env python3
"""
@author  : MG
@Time    : 2021/10/12 10:33
@File    : daily_task.py
@contact : mmmaaaggg@163.com
@desc    : 用于
"""
import logging

from tasks.wind import import_future_daily, import_future_min, \
    daily_to_model_server_db
from tasks.wind.future_reorg.reversion_rights_factor import task_save_adj_factor
from tasks.wind.to_vnpy import reversion_rights_factors_2_vnpy, wind_future_daily_2_vnpy, min_to_vnpy_increment

logger = logging.getLogger()


def _run_task():
    # DEBUG = True
    wind_code_set = None
    # import_future_info_hk(chain_param=None)
    # update_future_info_hk(chain_param=None)
    # import_future_info(chain_param=None)
    # # 导入期货每日行情数据
    import_future_daily(None, wind_code_set)
    # 根据商品类型将对应日线数据插入到 vnpy dbbardata 表中
    wind_future_daily_2_vnpy(start_vnp=True)
    # 重新计算复权数据
    task_save_adj_factor()
    reversion_rights_factors_2_vnpy()
    # 导入期货分钟级行情数据
    # 不可以只导入主力、次主力合约数据，将会导致数据发生缺失。
    # wind_code_set = get_main_secondary_contract_by_instrument_types()
    import_future_min(None, wind_code_set, recent_n_years=1)
    min_to_vnpy_increment()

    # 按品种合约倒叙加载每日行情
    # load_by_wind_code_desc(instrument_types=[
    #     ('RB', r"SHF"),
    #     ('I', r"DCE"),
    #     ('HC', r"SHF"),
    # ])
    logger.info("all task finished")


def run_daily_only(start_vnp=True):
    wind_code_set = None
    # 导入期货每日行情数据
    import_future_daily(None, wind_code_set)
    # 根据商品类型将对应日线数据插入到 vnpy dbbardata 表中
    wind_future_daily_2_vnpy(start_vnp=start_vnp)
    # 重新计算复权数据
    task_save_adj_factor()
    reversion_rights_factors_2_vnpy()


def run_daily_to_model_server_db():
    daily_to_model_server_db(instrument_types=['rb', 'hc', 'i', 'j', 'jm'])


if __name__ == "__main__":
    # import_future_info(None)
    # run_daily_only(start_vnp=True)
    # _test_get_main_secondary_contract_by_instrument_types()
    _run_task()
