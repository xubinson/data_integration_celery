#! /usr/bin/env python3
"""
@author  : MG
@Time    : 2021/9/30 9:00
@File    : restart_vnp.py
@contact : mmmaaaggg@163.com
@desc    : 用于
"""
import logging
import os
import subprocess
import time


def restart_vpn():
    """需要以管理员方式运行，否则启动vpn是会报 "OSError: [WinError 740] 请求的操作需要提升。" """
    logger = logging.getLogger(__name__)

    try:
        logger.info("关闭 vnp")
        os.system('taskkill /IM SSLChannel.exe /F')
        time.sleep(5)
        os.system('taskkill /IM SecureConnect.exe /F')
        time.sleep(5)
        logger.info("重新启动 vpn")
        # os.system(r"C:\Program Files (x86)\Hillstone\Hillstone Secure Connect\bin\SecureConnect.exe")
        # os.popen(r"C:\Program Files (x86)\Hillstone\Hillstone Secure Connect\bin\SecureConnect.exe")
        subprocess.Popen(r"C:\Program Files (x86)\Hillstone\Hillstone Secure Connect\bin\SecureConnect.exe",
                         cwd=r"C:\Program Files (x86)\Hillstone\Hillstone Secure Connect\bin"
                         )
        logger.info("重新启动 vpn 完成")
        time.sleep(5)
    except OSError:
        logger.exception("重新启动 vpn 异常")


if __name__ == "__main__":
    restart_vpn()
